# INRAeBeamerTheme

Adaptation of the INRA Beamer theme to INRAE
 
INRA Beamer theme originally adapted by François Guillaume : https://forge-dga.jouy.inra.fr/projects/inrabeamertheme
 
based on the LaTeX-Beamer package :
Copyright 2003 by Till Tantau <tantau@users.sourceforge.net>

This package can be redistributed and/or modified under the terms of the GNU Public License, version 2.
